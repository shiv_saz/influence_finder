import numpy as np
import matplotlib.pyplot as plt
from scipy import linalg as la

matrix = np.loadtxt('matrix.txt', dtype='int', ndmin=2)

eigen_val = la.eig(matrix, b=None, left=False, right=False, overwrite_a=False, overwrite_b=False, check_finite=False, homogeneous_eigvals=False)

alpha = round(1/max(eigen_val),4) - 0.0007

beta = 1

node_ranks = np.zeros((66,11),order='C')

y = 0

alpha_values = np.arange(0,alpha,0.004)

alpha_values = np.append(alpha_values,alpha)

for a in alpha_values:

	I_alpha_A = np.identity(66) - (a * matrix)

	inverse = la.inv(I_alpha_A)

	p = np.dot(matrix, inverse)

	rank = np.sum(p[0:len(p)],axis=1).tolist()

	for x in range(0,len(rank)):
		node_ranks[x,y] = rank[x]

	y += 1

for x in range(0,len(node_ranks)):
	plt.plot(alpha_values, node_ranks[x])

plt.xticks(np.arange(0,alpha,0.004))
plt.show()